﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется LotsOfSignals.rc
//
#define IDD_LOTSOFSIGNALS_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDC_Pusk                        1000
#define IDC_EDIT1                       1001
#define IDC_EDIT2                       1002
#define IDC_EDIT3                       1003
#define IDC_EDIT4                       1004
#define IDC_EDIT5                       1005
#define IDC_EDIT6                       1006
#define IDC_EDIT7                       1007
#define IDC_EDIT8                       1008
#define IDC_EDIT9                       1009
#define IDC_EDIT10                      1010
#define IDC_Input_Signal                1011
#define IDC_AKP                         1012
#define IDC_Pusk2                       1013
#define IDC_bpsk                        1014
#define IDC_msk                         1015
#define IDC_Sputnic2                    1016
#define IDC_Sputnic3                    1017
#define IDC_Sputnic4                    1018
#define IDC_Sputnic5                    1019
#define IDC_EDIT11                      1020
#define IDC_EDIT12                      1021
#define IDC_                            1022
#define IDC_Estimation                  1022

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
