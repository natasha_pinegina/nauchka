#pragma once

#include "Signal.h"

struct SignalGenerationParameters
{
	//метка времени начала
	double start_timestamp;
	//начальная фаза
	double start_phase;
	//продолжительность
	double duration;

	double n_samples;
	//частота дискретизации
	double sampling_frequency;
	//скорость передачи данных
	double bitrate;
	//дополнительный параметр
	double additional_parameter;
};

class SignalGenerator
{
public:
	void GenerateSignal(SignalType type, SignalGenerationParameters params, Signal& dst);
private:
	void GenerateBPSKSignal(SignalGenerationParameters params, Signal& dst);
	void GenerateMSKSignal(SignalGenerationParameters params, Signal& dst);
	bool RandomBit(double low_chance);
};