﻿
// LotsOfSignalsDlg.h: файл заголовка
//

#pragma once
#include <vector>
#include <complex>
using namespace std;

// Диалоговое окно CLotsOfSignalsDlg
class CLotsOfSignalsDlg : public CDialogEx
{
// Создание
public:
	CLotsOfSignalsDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_LOTSOFSIGNALS_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV

	CWnd* PicWnd_Input_Sign;
	CDC* PicDc_Input_Sign;
	CRect Pic_Input_Sign;

	CWnd* PicWnd_AKP;
	CDC* PicDc_AKP;
	CRect Pic_AKP;

	CPen koordpen, netkoordpen, signalpen, spectrpen, vsignalpen;
	CPen* pen;
	CFont fontgraph;
	CFont* font;

	CPen osi_pen;		// для осей 
	CPen setka_pen;		// для сетки
	CPen graf_pen;		// для графика функции
	CPen graf_pen2;
	CPen graf_pen3;

	double
		xx0,
		xxmax,
		yy0,
		yymax,
		xxi,
		yyi,
		iter;

	char
		znach[1000];

	double
		Max,
		Min,
		Mass1Min,
		Mass1Max,
		Mass2Min,
		Mass2Max;

	double xp, yp,			//коэфициенты пересчета
		xmin, xmax,			//максисимальное и минимальное значение х 
		ymin, ymax;			//максисимальное и минимальное значение y


	void Draw1Graph(std::vector<double>&, CDC*, CRect, CPen*, int, int, CString, CString);
	/*void Draw2Graph(float*, CPen*, float*, CPen*, CDC*, CRect, float, CString, CString);*/
	void Draw2Graph(std::vector<double>&, CPen*, std::vector<double>&, CPen*, CDC*, CRect, float, CString, CString);
	void DrawKoord(CDC*, CRect, CString, CString);
	//void Draw1Graph(std::vector<complex<double>>, CDC*, CRect, CPen*, int, int, CString, CString);

// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
	afx_msg double signal(double t);
	afx_msg  void correlate(std::vector<double>& signal1, std::vector<double>& signal2, vector<complex<double>> correlation, std::vector<double>& corr);


	double N=256;
	
public:
	double A1;
	double A2;
	double A3;
	double F1;
	double F2;
	double F3;
	double P1;
	double P2;
	double P3;
	double deltaT;
	afx_msg void OnBnClickedPusk();
	afx_msg void OnBnClickedPusk2();
	CButton bpsk;
	CButton msk;
	afx_msg void GenerateTwoLongSignal();


	//частота дискретизации
	double samplingFrequency = 300e6,
		//метка времени начала
		startTimestamp = 0,
		//продолжительность
		Duration = 1e-3,
		//начальная фаза
		startPhase = 0,
		nSamples = 0,
		//скорость передачи данных
		Bitrate = 100e5,
		//дополнительный параметр
		additionalParameter = 0;
	CButton Sputnic2;
	CButton Sputnic3;
	CButton Correlator;
	CButton Shema;
	double T1;
	double T2;
	afx_msg void OnBnClickedEstimation();
	std::vector<double> sIgnal2;
	std::vector<double> sIgnal3;
	std::vector<double> SSignal;
};
