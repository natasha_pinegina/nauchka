﻿
// LotsOfSignalsDlg.cpp: файл реализации
//



#define _USE_MATH_DEFINES
#include <math.h>
#include "pch.h"
#include "framework.h"
#include "LotsOfSignals.h"
#include "LotsOfSignalsDlg.h"
#include "afxdialogex.h"
#include <cmath>
#include <vector>
#include <complex>
#include "Generation.h"
#include "Methods.h"
//#include "Signal.h"
//#include "Generation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;
// Диалоговое окно CLotsOfSignalsDlg



CLotsOfSignalsDlg::CLotsOfSignalsDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_LOTSOFSIGNALS_DIALOG, pParent)
	, A1(3)
	, A2(2)
	, A3(4)
	, F1(0.05)
	, F2(0.06)
	, F3(0.07)
	, P1(0)
	, P2(0)
	, P3(0)
	, deltaT(5)
	, T1(0)
	, T2(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CLotsOfSignalsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT3, A1);
	DDX_Text(pDX, IDC_EDIT2, A2);
	DDX_Text(pDX, IDC_EDIT1, A3);
	DDX_Text(pDX, IDC_EDIT5, F1);
	DDX_Text(pDX, IDC_EDIT4, F2);
	DDX_Text(pDX, IDC_EDIT6, F3);
	DDX_Text(pDX, IDC_EDIT8, P1);
	DDX_Text(pDX, IDC_EDIT7, P2);
	DDX_Text(pDX, IDC_EDIT9, P3);
	DDX_Text(pDX, IDC_EDIT10, deltaT);
	DDX_Control(pDX, IDC_bpsk, bpsk);
	DDX_Control(pDX, IDC_msk, msk);
	DDX_Control(pDX, IDC_Sputnic2, Sputnic2);
	DDX_Control(pDX, IDC_Sputnic3, Sputnic3);
	DDX_Control(pDX, IDC_Sputnic4, Correlator);
	DDX_Control(pDX, IDC_Sputnic5, Shema);
	DDX_Text(pDX, IDC_EDIT12, T1);
	DDX_Text(pDX, IDC_EDIT11, T2);
}

BEGIN_MESSAGE_MAP(CLotsOfSignalsDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_Pusk, &CLotsOfSignalsDlg::OnBnClickedPusk)
	ON_BN_CLICKED(IDC_Pusk2, &CLotsOfSignalsDlg::OnBnClickedPusk2)
	ON_BN_CLICKED(IDC_Estimation, &CLotsOfSignalsDlg::OnBnClickedEstimation)
END_MESSAGE_MAP()


// Обработчики сообщений CLotsOfSignalsDlg

BOOL CLotsOfSignalsDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок


	PicWnd_Input_Sign = GetDlgItem(IDC_Input_Signal);
	PicDc_Input_Sign = PicWnd_Input_Sign->GetDC();
	PicWnd_Input_Sign->GetClientRect(&Pic_Input_Sign);

	PicWnd_AKP = GetDlgItem(IDC_AKP);
	PicDc_AKP = PicWnd_AKP->GetDC();
	PicWnd_AKP->GetClientRect(&Pic_AKP);


	setka_pen.CreatePen(		//для сетки
		PS_DOT,					//пунктирная
		0.1,						//толщина 1 пиксель
		RGB(0, 0, 250));		//цвет  черный
	osi_pen.CreatePen(			//координатные оси
		PS_SOLID,				//сплошная линия
		3,						//толщина 2 пикселя
		RGB(0, 0, 0));			//цвет черный

	graf_pen.CreatePen(			//график
		PS_SOLID,				//сплошная линия
		2,						//толщина 2 пикселя
		RGB(0, 0, 255));			//цвет черный
	graf_pen2.CreatePen(PS_SOLID, 2, RGB(255, 0, 0));
	graf_pen3.CreatePen(PS_SOLID, 2, RGB(155, 150, 150));
	
	CButton* pcb1 = (CButton*)(this->GetDlgItem(IDC_bpsk));
	pcb1->SetCheck(1);

	CButton* pcb2 = (CButton*)(this->GetDlgItem(IDC_Sputnic2));
	pcb2->SetCheck(1);

	CButton* pcb3 = (CButton*)(this->GetDlgItem(IDC_Sputnic4));
	pcb3->SetCheck(1);
	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CLotsOfSignalsDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CLotsOfSignalsDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CLotsOfSignalsDlg::Draw1Graph(std::vector<double>& Mass, CDC* WinDc, CRect WinPic, CPen* graphpen, int KolToch, int AbsMax, CString Abs, CString Ord)
{
	// поиск максимального и минимального значения
	xmin = Mass[0];
	xmax = Mass[0];
	for (int i = 0; i < Mass.size(); i++)
	{
		if (Mass[i] < xmin)
		{
			xmin = Mass[i];
		}
		if (Mass[i] > xmax)
		{
			xmax = Mass[i];
		}
	}
	/*int itermax = 0;
	double mean = 0;
	for (unsigned int i = 0; i < Mass.size(); i++)
	{
		double curr = Mass[i].real() * Mass[i].real() + Mass[i].imag() * Mass[i].imag();
		if (curr > mean)
		{
			itermax = i;
			mean = curr;
		}
	}
	xmax = Mass[itermax];*/

	//---- отрисовка -------------------------------------------------
	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);
	bmp.CreateCompatibleBitmap(
		WinDc,
		WinPic.Width(),
		WinPic.Height());
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);
	//------ заливка фона графика белым цветом ---------------------------------
	MemDc->FillSolidRect(
		WinPic,
		RGB(
			255,
			255,
			255));
	//------ отрисовка сетки координат -----------------------------------------
	pen = MemDc->SelectObject(&setka_pen);
	// вертикальные линии сетки координат
	for (float i = (float)WinPic.Width() / 25; i < WinPic.Width(); i += (float)WinPic.Width() / 25)
	{
		MemDc->MoveTo(i, 0);
		MemDc->LineTo(i, WinPic.Height());
	}
	// горизонтальные линии сетки координат
	for (float i = (float)WinPic.Height() / 10; i < WinPic.Height(); i += (float)WinPic.Height() / 10)
	{
		MemDc->MoveTo(0, i);
		MemDc->LineTo(WinPic.Width(), i);
	}
	//------ отрисовка осей ----------------------------------------------------
	pen = MemDc->SelectObject(&koordpen);
	// отрисовка оси X
	MemDc->MoveTo(2, (float)WinPic.Height() * 9 / 10);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	MemDc->MoveTo(WinPic.Width() - 15, (float)WinPic.Height() * 9 / 10 + 2);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	MemDc->MoveTo(WinPic.Width() - 15, (float)WinPic.Height() * 9 / 10 - 2);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	// деления на оси X
	for (float i = (float)WinPic.Width() / 25; i < WinPic.Width() * 24 / 25; i += (float)WinPic.Width() / 25)
	{
		MemDc->MoveTo(i, WinPic.Height() * 9 / 10 + 2);
		MemDc->LineTo(i, WinPic.Height() * 9 / 10 - 3);
	}
	// отрисовка оси Y
	MemDc->MoveTo(WinPic.Width() * 2 / 25, WinPic.Height() - 2);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	MemDc->MoveTo(WinPic.Width() * 2 / 25 - 2, 15);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	MemDc->MoveTo(WinPic.Width() * 2 / 25 + 2, 15);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	// деления на оси Y
	for (float i = (float)WinPic.Height() / 5; i < WinPic.Height(); i += (float)WinPic.Height() / 10)
	{
		MemDc->MoveTo(WinPic.Width() * 2 / 25 - 2, i);
		MemDc->LineTo(WinPic.Width() * 2 / 25 + 3, i);
	}
	//------ подписи осей --------------------------------------------
	// установка прозрачного фона текста
	MemDc->SetBkMode(TRANSPARENT);
	// установка шрифта
	MemDc->SelectObject(&fontgraph);
	// подпись оси X
	MemDc->TextOut((float)WinPic.Width() * 24 / 25 + 4, (float)WinPic.Height() * 9 / 10 + 2, Abs);
	// подпись оси Y
	MemDc->TextOut((float)WinPic.Width() * 2 / 25 + 5, 0, Ord);
	// выбор области для рисования
	xx0 = WinPic.Width() * 2 / 25;
	xxmax = WinPic.Width() * 24 / 25;
	yy0 = WinPic.Height() / 10;
	yymax = WinPic.Height() * 9 / 10;
	// отрисовка
	pen = MemDc->SelectObject(graphpen);
	MemDc->MoveTo(xx0, yymax + (Mass[0] - xmin) / (xmax - xmin) * (yy0 - yymax));
	for (int i = 0; i < Mass.size(); i++)
	{
		xxi = xx0 + (xxmax - xx0) * i / (Mass.size() - 1);
		yyi = yymax + (Mass[i] - xmin) / (xmax - xmin) * (yy0 - yymax);
		MemDc->LineTo(xxi, yyi);
	}
	//----- вывод числовых значений -----------------------------------
	// по оси абсцисс
	//for (int i = 6; i < 25; i += 5)
	//{
	//	sprintf(znach, "%5.1f", (i - 1) * (float)AbsMax / 22);
	//	MemDc->TextOut(i * WinPic.Width() / 25 + 2, WinPic.Height() * 9 / 10 + 2, CString(znach));
	//}
	//// по оси ординат
	//sprintf(znach, "%5.2f", xmax);
	//MemDc->TextOut(0, WinPic.Height() / 20 + 1, CString(znach));
	//sprintf(znach, "%5.2f", 0.75 * xmax);
	//MemDc->TextOut(0, WinPic.Height() * 5 / 20 + 1, CString(znach));
	//sprintf(znach, "%5.2f", 0.5 * xmax);
	//MemDc->TextOut(0, WinPic.Height() * 9 / 20 + 1, CString(znach));
	//sprintf(znach, "%5.2f", 0.25 * xmax);
	//MemDc->TextOut(0, WinPic.Height() * 13 / 20 + 1, CString(znach));
	//sprintf(znach, "%5.2f", 0.0);
	//MemDc->TextOutW(0, WinPic.Height() * 9 / 10 + 2, CString(znach));
	//----- вывод на экран -------------------------------------------
	WinDc->BitBlt(0, 0, WinPic.Width(), WinPic.Height(), MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void CLotsOfSignalsDlg::Draw2Graph(std::vector<double>& Mass1, CPen* graph1pen, std::vector<double>& Mass2, CPen* graph2pen, CDC* WinDc, CRect WinPic, float AbsMax, CString Abs, CString Ord)
{
	//----- поиск максимального и минимального значения -----------------------------
	Mass1Min = Mass1[0];
	Mass1Max = Mass1[0];
	Mass2Min = Mass2[0];
	Mass2Max = Mass2[0];
	for (int i = 1; i < N; i++)
	{
		if (Mass1[i] < Mass1Min)
		{
			Mass1Min = Mass1[i];
		}
		if (Mass1[i] > Mass1Max)
		{
			Mass1Max = Mass1[i];
		}
		/*if (Mass2[i] < Mass2Min)
		{
			Mass2Min = Mass2[i];
		}
		if (Mass2[i] > Mass2Max)
		{
			Mass2Max = Mass2[i];
		}*/
	}
	for (int i = 0; i < N / 4; i++)
	{
		if (Mass2[i] < Mass2Min)
		{
			Mass2Min = Mass2[i];
		}
		if (Mass2[i] > Mass2Max)
		{
			Mass2Max = Mass2[i];
		}
	}
	if (Mass2Max > Mass1Max)
	{
		Max = Mass2Max;
	}
	else
	{
		Max = Mass1Max;
	}
	if (Mass2Min < Mass1Min)
	{
		Min = Mass2Min;
	}
	else
	{
		Min = Mass1Min;
	}
	//---- отрисовка -------------------------------------------------
	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);
	bmp.CreateCompatibleBitmap(
		WinDc,
		WinPic.Width(),
		WinPic.Height());
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);
	//------ заливка фона графика белым цветом ---------------------------------
	MemDc->FillSolidRect(
		WinPic,
		RGB(
			255,
			255,
			255));
	//------ отрисовка сетки координат -----------------------------------------
	pen = MemDc->SelectObject(&setka_pen);
	// вертикальные линии сетки координат
	for (float i = (float)WinPic.Width() / 25; i < WinPic.Width(); i += (float)WinPic.Width() / 25)
	{
		MemDc->MoveTo(i, 0);
		MemDc->LineTo(i, WinPic.Height());
	}
	// горизонтальные линии сетки координат
	for (float i = (float)WinPic.Height() / 10; i < WinPic.Height(); i += (float)WinPic.Height() / 10)
	{
		MemDc->MoveTo(0, i);
		MemDc->LineTo(WinPic.Width(), i);
	}
	//------ отрисовка осей ----------------------------------------------------
	pen = MemDc->SelectObject(&osi_pen);
	// отрисовка оси X
	MemDc->MoveTo(2, (float)WinPic.Height() * 9 / 10);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	MemDc->MoveTo(WinPic.Width() - 15, (float)WinPic.Height() * 9 / 10 + 2);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	MemDc->MoveTo(WinPic.Width() - 15, (float)WinPic.Height() * 9 / 10 - 2);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	// деления на оси X
	for (float i = (float)WinPic.Width() / 25; i < WinPic.Width() * 24 / 25; i += (float)WinPic.Width() / 25)
	{
		MemDc->MoveTo(i, (float)WinPic.Height() * 9 / 10 + 2);
		MemDc->LineTo(i, (float)WinPic.Height() * 9 / 10 - 3);
	}
	// отрисовка оси Y
	MemDc->MoveTo(WinPic.Width() * 2 / 25, WinPic.Height() - 2);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	MemDc->MoveTo(WinPic.Width() * 2 / 25 - 2, 15);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	MemDc->MoveTo(WinPic.Width() * 2 / 25 + 2, 15);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	// деления на оси Y
	for (float i = (float)WinPic.Height() / 5; i < WinPic.Height(); i += (float)WinPic.Height() / 10)
	{
		MemDc->MoveTo(WinPic.Width() * 2 / 25 - 2, i);
		MemDc->LineTo(WinPic.Width() * 2 / 25 + 3, i);
	}
	//------ подписи осей --------------------------------------------
	// установка прозрачного фона текста
	MemDc->SetBkMode(TRANSPARENT);
	// установка шрифта
	MemDc->SelectObject(&fontgraph);
	// подпись оси X
	MemDc->TextOut((float)WinPic.Width() * 24 / 25 + 4, (float)WinPic.Height() / 2 + 2, Abs);
	// подпись оси Y
	MemDc->TextOut((float)WinPic.Width() * 2 / 25 + 5, 0, Ord);
	// выбор области для рисования
	xx0 = WinPic.Width() * 2 / 25;
	xxmax = WinPic.Width() * 24 / 25;
	yy0 = WinPic.Height() / 10;
	yymax = WinPic.Height() * 9 / 10;
	// отрисовка первого графика
	pen = MemDc->SelectObject(graph1pen);
	MemDc->MoveTo(xx0, yymax + (Mass1[0] - Min) / (Max - Min) * (yy0 - yymax));
	for (int i = 0; i < N; i++)
	{
		xxi = xx0 + (xxmax - xx0) * i / (N - 1);
		yyi = yymax + (Mass1[i] - Min) / (Max - Min) * (yy0 - yymax);
		MemDc->LineTo(xxi, yyi);
	}
	// отрисовка второго графика
	pen = MemDc->SelectObject(graph2pen);
	MemDc->MoveTo(xx0, yymax + (Mass2[0] - Min) / (Max - Min) * (yy0 - yymax));
	for (int i = 0; i < N/4; i++)
	{
		xxi = xx0 + (xxmax - xx0) * i / (N - 1);
		yyi = yymax + (Mass2[i] - Min) / (Max - Min) * (yy0 - yymax);
		MemDc->LineTo(xxi, yyi);
	}
	//----- вывод на экран -------------------------------------------
	WinDc->BitBlt(0, 0, WinPic.Width(), WinPic.Height(), MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

double CLotsOfSignalsDlg::signal(double t)
{
	double Amplitude[] = { A1, A2, A3 };
	double Frequency[] = { F1, F2, F3 };
	double Phase[] = { P1, P2, P3 };
	double result = 0;
	for (int i = 0; i <= 2; i++)
	{
		result += Amplitude[i] * sin(2 * /*M_PI*/3.14159 * Frequency[i] * t + Phase[i]);
	}
	return result;
}


void CLotsOfSignalsDlg::correlate(std::vector<double>& signal1, std::vector<double>& signal2, vector<complex<double>> correlation, vector<double>& corr)
{
	for (unsigned int n = 0; n < (signal1.size() - signal2.size()); n++)
	{
		complex<double> counter = complex<double>(0, 0);
		for (unsigned int m = 0; m < signal2.size(); m++)
		{
			if ((m + n) >= 0 && (m + n) < signal1.size())
			{
				counter += signal2[m] * conj(signal1[m + n]);
			}
			else continue;
		}
		counter /= (double)signal1.size();
		correlation.push_back(counter);
		corr.push_back(correlation[n].real() * correlation[n].real() + correlation[n].imag() * correlation[n].imag());
		//correlation.push_back(n);
	}
}


void CLotsOfSignalsDlg::OnBnClickedPusk()
{
	UpdateData(true);
	std::vector<double> signal1;
	std::vector<double> signal2;
	for (double i = 0; i < N; i++)
	{
		signal1.push_back(signal(i));
		//signal2.push_back(Signal(i+deltaT));
	}
	for (double i = 0; i < N/4; i++)
	{
		signal2.push_back(signal(i+deltaT));
	}
	Draw2Graph(signal1, &graf_pen2, signal2, &graf_pen3, PicDc_Input_Sign, Pic_Input_Sign, N, CString("x"),
		CString("Ampl"));
	std::vector<complex<double>> cor;
	std::vector<double> curr;
	correlate(signal1,signal2,cor,curr);
	for (double i = 0; i < cor.size(); i++)
	{
		curr.push_back(cor[i].real() * cor[i].real() + cor[i].imag() * cor[i].imag());
	}
	Draw1Graph(curr, PicDc_AKP, Pic_AKP,&graf_pen3,N,N,CString("x"),CString("Ampl"));
}



void CLotsOfSignalsDlg::OnBnClickedPusk2()
{
	//vector<complex<double>> signaL;
	//vector<double> Key;
	//std::vector<double> sIgnal;

	//SignalType signal_type;
	//if (bpsk.GetCheck())
	//	signal_type = BPSK;
	//if (msk.GetCheck())
	//	signal_type = MSK;

	//Signal ssignal = { signaL,Key," "," ",samplingFrequency,startTimestamp,Duration};
	//SignalGenerationParameters parametrs = { 
	//	startTimestamp,
	//	startPhase,
	//	Duration,
	//	nSamples,
	//	samplingFrequency,
	//	Bitrate,
	//	additionalParameter};

	//SignalGenerator s;
	//s.GenerateSignal(signal_type, parametrs, ssignal);
	//
	//Signal ssignal2, ssignal3, ssignal23summ, correlation12;
	//Methods::transformSignal(ssignal, Duration / 10, Duration/10, 0, 1, 10, ssignal2);
	//Methods::transformSignal(ssignal, 2*Duration / 10, Duration / 10, 0, 1, 10, ssignal3);
	//Methods::coherentSumm({ ssignal2 , ssignal3 }, ssignal23summ);
	//
	//Methods::correlate(ssignal, ssignal23summ, correlation12);


	//for (int i = 0; i < ssignal.signal.size(); i++)
	//{
	//	sIgnal.push_back(correlation12.signal[i].real() * correlation12.signal[i].real() + correlation12.signal[i].imag() * correlation12.signal[i].imag());
	//}

	//Draw1Graph(sIgnal, PicDc_Input_Sign, Pic_Input_Sign, &graf_pen3, N, N, CString("x"), CString("Ampl"));
	GenerateTwoLongSignal();
}

void CLotsOfSignalsDlg::GenerateTwoLongSignal()
{
	std::vector<double> SSignal;
	std::vector<double> SSignal2;
	std::vector<double> SSignal3;
	sIgnal2.clear();
	sIgnal3.clear();
	SSignal.clear();

	SignalType signal_type;
	if (bpsk.GetCheck())
		signal_type = BPSK;
	if (msk.GetCheck())
		signal_type = MSK;

	samplingFrequency = 300e6;
	Signal LongSignal1, LongSignal2;
	SignalGenerationParameters parametrs = {startTimestamp,startPhase,Duration,nSamples,
		samplingFrequency,Bitrate,additionalParameter };

	SignalGenerator ss;
	ss.GenerateSignal(signal_type, parametrs, LongSignal1);
	
	
		//метка времени начала
		startTimestamp = 200,
		//продолжительность
		Duration = 1e-3,
		//скорость передачи данных
		Bitrate = 100e5,
		startPhase = 2*3.14159;

	parametrs = { startTimestamp,startPhase,Duration,nSamples,
		samplingFrequency,Bitrate,additionalParameter };

	ss.GenerateSignal(signal_type, parametrs, LongSignal2);

	Signal signal11, signal21,SummSignal1;//сигналы к базовому спутнику и их сумма
	Methods::transformSignal(LongSignal1, 1*Duration / 10, Duration / 10, 0, 1, 10, signal11);
	Methods::transformSignal(LongSignal2, 2*Duration / 10, Duration / 10, 0, 1, 10, signal21);
	Methods::coherentSumm({ signal11 , signal21 }, SummSignal1);

	Signal signal12, signal22, SummSignal2;//сигналы к 2 спутнику и их сумма
	Methods::transformSignal(LongSignal1, 1.5*Duration / 10, Duration / 100, 0, 1, 10, signal12);
	Methods::transformSignal(LongSignal2, 2.25* Duration / 10, Duration / 100, 0, 1, 10, signal22);
	Methods::coherentSumm({ signal12 , signal22 }, SummSignal2);

	Signal signal13, signal23, SummSignal3;//сигналы к 3 спутнику и их сумма
	Methods::transformSignal(LongSignal1, 1.1*Duration / 10, Duration / 100, 0, 1, 10, signal13);
	Methods::transformSignal(LongSignal2, 2.5 * Duration / 10, Duration / 100, 0, 1, 10, signal23);
	Methods::coherentSumm({ signal13 , signal23 }, SummSignal3);

	Signal correlate12, correlate13;
	//Корреляция первых двух сигналов
	Methods::correlate(SummSignal1, SummSignal2, correlate12);
	//Корреляция первого сигнала со вторым
	Methods::correlate(SummSignal1, SummSignal3, correlate13);

	//Модуль этих корреляций
	for (int i = 0; i < correlate12.signal.size(); i++)
	{
		sIgnal2.push_back(correlate12.signal[i].real() * correlate12.signal[i].real() + correlate12.signal[i].imag() * correlate12.signal[i].imag());
		sIgnal3.push_back(correlate13.signal[i].real() * correlate13.signal[i].real() + correlate13.signal[i].imag() * correlate13.signal[i].imag());
	}

	double t2i = 0,otshetSignala=0;
	//Находим максимум первой корреляции
	for (int i = 0; i < sIgnal2.size(); i++)
	{
		if (otshetSignala < sIgnal2[i])
		{
			t2i = i;
			otshetSignala = sIgnal2[i];
		}
	}
	T1 = t2i;

	//Сдивг второго сигнала на величину t2i
	std::vector<double> ShiftedSecondSignal;
	int iter = 0;
	for (int i = 0; i < sIgnal2.size() + t2i; i++)
	{
		if (i < t2i)ShiftedSecondSignal.push_back(0);
		else
		{
			ShiftedSecondSignal.push_back(sIgnal2[iter]);
			iter++;
		}
	}
	

	//Модуль сигнала на базовом спутнике
	for (int i = 0; i < SummSignal1.signal.size(); i++)
	{
		SSignal.push_back(SummSignal1.signal[i].real() * SummSignal1.signal[i].real() + SummSignal1.signal[i].imag() * SummSignal1.signal[i].imag());
	}

	double LenghtS1 = SSignal.size();
	double LenghtShiftedSecondSignal = ShiftedSecondSignal.size();
	FILE* results1;
	if (fopen_s(&results1, "Help.txt", "w") == 0)
	{
			fprintf(results1, "%+*.1f ", 10, LenghtS1);
			fprintf(results1, "\n");
			fprintf(results1, "%+*.1f ", 10, LenghtShiftedSecondSignal);
			fprintf(results1, "\n");
	}
	//Суммируем сдвинутый сигал и базовый
	std::vector<double> SumOf1SignalAndShifted;
	for (int i = 0; i < LenghtShiftedSecondSignal; i++)
	{
		if (i >= LenghtS1)SumOf1SignalAndShifted.push_back(ShiftedSecondSignal[i]);
		else SumOf1SignalAndShifted.push_back(ShiftedSecondSignal[i]- SSignal[i]);
	}

	//Коррелируем третий сигнал с только что пролученной суммой
	std::vector<complex<double>> cor;
	std::vector<double> correlationOfTheThirdSignalWithTheSum;
	correlate(SumOf1SignalAndShifted, sIgnal3, cor, correlationOfTheThirdSignalWithTheSum);
	for (double i = 0; i < cor.size(); i++)
	{
		correlationOfTheThirdSignalWithTheSum.push_back(cor[i].real() * cor[i].real() + cor[i].imag() * cor[i].imag());
	}
	//Суммируем третий сигнал с только что полученной корреляцией
	double LenghtCor = correlationOfTheThirdSignalWithTheSum.size();
	double LenghtS3 = sIgnal3.size();
	FILE* results2;
	if (fopen_s(&results2, "Help2.txt", "w") == 0)
	{
		fprintf(results2, "%+*.1f ", 10, LenghtCor);
		fprintf(results2, "\n");
		fprintf(results2, "%+*.1f ", 10, LenghtS3);
		fprintf(results2, "\n");
	}

	std::vector<double> Result;
	for (int i = 0; i < LenghtS3; i++)
	{
		if (i >= LenghtCor)Result.push_back(sIgnal3[i]);
		else Result.push_back(correlationOfTheThirdSignalWithTheSum[i] - sIgnal3[i]);
	}

	//Находим второй отсчет
	double NewOtshetSignala = 0, t3i = 0;
	for (int i = 0; i < Result.size(); i++)
	{
		if (NewOtshetSignala < Result[i])
		{
			t3i = i;
			NewOtshetSignala = Result[i];
		}
	}
	T2 = t3i;
	UpdateData(false);
	for (int i = 0; i < SummSignal2.signal.size(); i++)
	{
		SSignal2.push_back(SummSignal2.signal[i].real() * SummSignal2.signal[i].real() + SummSignal2.signal[i].imag() * SummSignal2.signal[i].imag());
		SSignal3.push_back(SummSignal3.signal[i].real() * SummSignal3.signal[i].real() + SummSignal3.signal[i].imag() * SummSignal3.signal[i].imag());
	}
	//Отрисовка сигнала и корреляции
	if(Correlator.GetCheck())
	{ 
	if (Sputnic2.GetCheck())
	{
		Draw1Graph(sIgnal2/*Result*/, PicDc_AKP, Pic_AKP, &graf_pen3, N, sIgnal2/*Result*/.size(), CString("x"), CString("Ampl"));
		Draw2Graph(SSignal, &graf_pen2, SSignal2, &graf_pen, PicDc_Input_Sign, Pic_Input_Sign, N, CString("x"),
			CString("Ampl"));
	}
		if (Sputnic3.GetCheck())
		{ 
		Draw1Graph(sIgnal3, PicDc_AKP, Pic_AKP, &graf_pen3, N, sIgnal3.size(), CString("x"), CString("Ampl"));
		Draw2Graph(SSignal, &graf_pen2, SSignal3, &graf_pen, PicDc_Input_Sign, Pic_Input_Sign, N, CString("x"),
			CString("Ampl"));
		}
	}
	if (Shema.GetCheck())
	{
		Draw1Graph(/*sIgnal2*/Result, PicDc_AKP, Pic_AKP, &graf_pen3, N, /*sIgnal2*/Result.size(), CString("x"), CString("Ampl"));
	}

}


void CLotsOfSignalsDlg::OnBnClickedEstimation()
{
	double t2i = T1;
	double por =1/Bitrate;
	std::vector<double> Criterion;
	for(double k=0; k<1; k+=0.2)
	{ 
		//Сдвиг второго сигнала на величину t2i
		std::vector<double> ShiftedSecondSignal;
		int iter = 0;
		for (int i = 0; i < sIgnal2.size() + t2i; i++)
		{
			if (i < t2i)ShiftedSecondSignal.push_back(0);
			else
			{
				ShiftedSecondSignal.push_back(sIgnal2[iter]);
				iter++;
			}
		}

		double LenghtS1 = SSignal.size();
		double LenghtShiftedSecondSignal = ShiftedSecondSignal.size();
		FILE* results1;
		if (fopen_s(&results1, "Help.txt", "w") == 0)
		{
			fprintf(results1, "%+*.1f ", 10, LenghtS1);
			fprintf(results1, "\n");
			fprintf(results1, "%+*.1f ", 10, LenghtShiftedSecondSignal);
			fprintf(results1, "\n");
		}
		//Суммируем сдвинутый сигал и базовый
		std::vector<double> SumOf1SignalAndShifted;
		for (int i = 0; i < LenghtShiftedSecondSignal; i++)
		{
			if (i >= LenghtS1)SumOf1SignalAndShifted.push_back(ShiftedSecondSignal[i]);
			else SumOf1SignalAndShifted.push_back(ShiftedSecondSignal[i] - SSignal[i]);
		}

		//Коррелируем третий сигнал с только что пролученной суммой
		std::vector<complex<double>> cor;
		std::vector<double> correlationOfTheThirdSignalWithTheSum;
		correlate(SumOf1SignalAndShifted, sIgnal3, cor, correlationOfTheThirdSignalWithTheSum);
		for (double i = 0; i < cor.size(); i++)
		{
			correlationOfTheThirdSignalWithTheSum.push_back(cor[i].real() * cor[i].real() + cor[i].imag() * cor[i].imag());
		}
		//Суммируем третий сигнал с только что полученной корреляцией
		double LenghtCor = correlationOfTheThirdSignalWithTheSum.size();
		double LenghtS3 = sIgnal3.size();
		FILE* results2;
		if (fopen_s(&results2, "Help2.txt", "w") == 0)
		{
			fprintf(results2, "%+*.1f ", 10, LenghtCor);
			fprintf(results2, "\n");
			fprintf(results2, "%+*.1f ", 10, LenghtS3);
			fprintf(results2, "\n");
		}

		std::vector<double> Result;
		for (int i = 0; i < LenghtS3; i++)
		{
			if (i >= LenghtCor)Result.push_back(sIgnal3[i]);
			else Result.push_back(correlationOfTheThirdSignalWithTheSum[i] - sIgnal3[i]);
		}

		//Находим второй отсчет
		double NewOtshetSignala = 0, t3i = 0;
		for (int i = 0; i < Result.size(); i++)
		{
			if (NewOtshetSignala < Result[i])
			{
				t3i = i;
				NewOtshetSignala = Result[i];
			}
		}
		t2i += por;
		//тут что-то не так(разные длины векторов, t3i лежит за пределами второго вектора)
		Criterion.push_back(sIgnal3[t3i]/ correlationOfTheThirdSignalWithTheSum[t3i]);
	}
	Draw1Graph(Criterion, PicDc_AKP, Pic_AKP, &graf_pen3, N, Criterion.size(), CString("x"), CString("Ampl"));
}
